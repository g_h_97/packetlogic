import matplotlib.pyplot as plt
import ast

for i in range(0,3):
    trafficArray = ast.literal_eval(open('traffic_array_{}'.format(i), 'r').read())
    trafficIntArray = list(map(int, trafficArray))

# Check traffic differance by some percentage
# to signal a traffic issue
print(trafficIntArray[0], trafficIntArray[-1])
warnThresh = trafficIntArray[0]/10
if trafficIntArray[0]-trafficIntArray[-1] >= warnThresh:
    print("TRAFFIC ISSUE DETECTED, PLEASE CHECK THE NETWORK !!!")

#plt.plot(trafficIntArray, color='magenta', marker='o',mfc='pink' ) #plot the data
plt.plot(trafficIntArray, color='orange')
plt.xticks(range(0,len(trafficIntArray)+1, 1)) #set the tick frequency on x-axis

plt.ylabel('Traffic') #set the label for y axis
plt.xlabel('Time') #set the label for x-axis
plt.title("Plotting a list") #set the title of the graph
plt.show() #display the graph
plt.save()

#plt.plot(trafficArray)
#plt.xticks(range(0,len(trafficArray)+1, 1))

#plt.show()
